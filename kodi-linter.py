"""lint and cleanup kodi movie and tv directories"""
# pylint:disable=c0103

import glob
import json
import os
# import sys

import click

DB_LINTER = 'kodi-linter.json'
DB_ACTIONS = 'kodi-actions.json'


@click.group()
def cli():
    """grouping of cli commands"""

@cli.command()
def initdb():
    """init the db"""
    click.echo('Initialized the database')

@cli.command()
def dropdb():
    """drop the db"""
    click.echo('Dropped the database')

@cli.command()
@click.argument('pth')
def catalog(pth):
    """update the db by cataloging the indicated share"""
    # pth = '/run/user/1000/gvfs/smb-share:server=diskstation,share=video/Grown Ups/'
    pth = os.path.abspath(pth)
    if pth[-1] != os.sep:
        pth += os.sep

    db = load_db(DB_LINTER)
    lst = []
    click.echo("Accessing %s" % pth)
    for fname in glob.glob(pth+'**', recursive=True):
        if not os.path.isfile(fname):
            continue
        path = os.path.abspath(fname)
        lst.append(path)
        click.echo(path)
    lst.sort()
    db[pth] = lst
    save_db(db, DB_LINTER)


@cli.command()
def view():
    """output the db to the screen"""
    db = load_db()
    for share in db.keys():
        for path in db[share]:
            this = Lintee(path, share)
            click.echo(this)


@cli.command()
@click.option('--no-action', 'take_action', flag_value=False, default=True)
@click.option('--mismatch', 'mismatch', flag_value=True, default=False)
def lint(take_action, mismatch):
    '''filter on title not matching parent directory'''
    db = load_db(DB_LINTER)
    actions = load_db(DB_ACTIONS)
    for share in db.keys():
        previous = None
        for path in db[share]:
            this = Lintee(path, share)
            if (previous is not None) and this.base != previous.base:
                if take_action:
                    click.clear()
            if this.exists_in_root():
                click.echo('Exists in Root -> %s' % this)
                if take_action:
                    actions += get_action(this, previous)
            elif this.parent_base_mismatch():
                if mismatch:
                    click.echo('Name Mismatch  -> %s' % this)
                    if take_action:
                        actions += get_action(this, previous)
            previous = this  # set previous to this
            # click.echo(actions)
    if take_action:
        click.echo('Actions:')
        for action in actions:
            click.echo(action)
        save_db(actions, DB_ACTIONS)


@cli.command()
@click.option('--dry-run', 'dry_run', flag_value=True, default=False)
def do(dry_run):
    '''peform the actions queued in the actions db'''
    actions = load_db(DB_ACTIONS)
    while actions:
        action = actions.pop(0)
        if action[0] == 'mkdir':
            cmd, target = action
            click.echo("os.%s('%s')" % (cmd, target))
            if not dry_run:
                os.mkdir(target)
                save_db(actions, DB_ACTIONS)

        elif action[0] == 'rename':
            cmd, source, target = action
            click.echo("os.%s('%s', '%s')" % (cmd, source, target))
            if not dry_run:
                os.rename(source, target)
                save_db(actions, DB_ACTIONS)

        else:
            click.echo(action)

class Lintee:
    '''object to breakdown a path into its relevant bits'''
    def __init__(self, path, share):
        self._share = share
        self._path = path
        self.parent, self.filename = path.split(os.sep)[-2:]
        if os.path.join(self._share, self.filename) == self._path:
            self.parent = ''
        self._base, self.ext = os.path.splitext(self.filename)
        self.old_base = self._base

    @property
    def base(self):
        '''the file's base name [parent/base.ext]'''
        return self._base
    @base.setter
    def base(self, val):
        self._base = val

    @property
    def path(self):
        '''return original path'''
        return self._path
    @property
    def new_path(self):
        '''return modified path'''
        return os.path.join(self._share, self.parent, self.base + self.ext)
    @property
    def share(self):
        '''return the objects share'''
        return self._share

    def __str__(self):
        return "[%s] [%s] [%s]" % (
                        self.parent, self._base, self.ext)

    def parent_base_mismatch(self):
        '''return true if parent != base'''
        return self.parent != self._base

    def exists_in_root(self):
        '''return True if file exists in root of share'''
        return self.parent == ''



def load_db(dbname='kodi-linter.json'):
    '''load the db and return as a python dictionary'''
    try:
        db = json.loads(open(dbname).read())
    except FileNotFoundError:
        if dbname == DB_LINTER:
            db = {}
        else:
            db = []
    return db

def save_db(db, dbname='kodi-linter.json'):
    '''persist the db (dict) as a json file'''
    with open(dbname, 'w') as hout:
        json.dump(db, hout)

def get_action(lnte, prev_lnte=None):
    '''determine what should be done'''
    click.echo('previous: %s' % prev_lnte)
    click.echo('current : %s' % lnte)
    if prev_lnte is not None and (prev_lnte.old_base != lnte.base):
        prev_lnte = None
    acts = []
    while True:
        if lnte.parent:
            click.echo('1 - Rename file to [%s]' % lnte.parent)
            click.echo('2 - Edit file name based on [%s]' % lnte.parent)
        else:
            click.echo('m - Make dir and rename file into it')
            if prev_lnte is not None:
                click.echo('v - Move a file into last dir [%s]' % prev_lnte.parent)

        click.echo('3 - Edit file name [%s]' % lnte.base)
        click.echo('i - Search IMDB for [%s]' % lnte.base)
        click.echo('s - No Change to file name')
        click.echo('q - Exit Lint process')
        click.echo('? ', nl=False)
        rsp = click.getchar(echo=True)
        click.echo()
        if lnte.parent:
            if rsp == '1':
                lnte.base = lnte.parent
                click.echo(lnte.path)
                click.echo(lnte.new_path)
                acts.append(('rename', lnte.path, lnte.new_path))
                return acts

        if rsp in ('m', 'v'):
            if rsp == 'm':
                new_dir = click.prompt('name for new directory ')
                if new_dir:
                    click.echo('mkdir %s' % new_dir)
                    acts.append(('mkdir', os.path.join(lnte.share,new_dir)))
                lnte.parent = new_dir
                lnte.base = new_dir
                acts.append(('rename', lnte.path, lnte.new_path))
                click.echo(lnte.path)
                click.echo(lnte.new_path)
            if rsp == 'v':
                lnte.parent = prev_lnte.parent
                lnte.base = prev_lnte.base
                acts.append(('rename', lnte.path, lnte.new_path))

            return acts

        if rsp == 's':
            click.echo('No Change')
            return acts
        if rsp == 'i':
            click.launch('https://www.imdb.com/find?q=%s' % lnte.base)
        if rsp == '3':
            lnte.base = click.prompt('edit ', default=lnte.base)
            acts.append(('rename', lnte.path, lnte.new_path))
            return acts
        if rsp == 'q':
            click.confirm('Do you want to continue?', abort=True)


if __name__ == "__main__":
    cli()
